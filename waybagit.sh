#!/bin/bash

# Waybagit uses historical Wayback Machine snapshots to build a local Git repository.

# Usage:
# * Store domain names in domains.txt
# * Run script: # ./waybagit
# * Navigate through Git repository in local folder: # git log -p

# This script is work in progress. Obviously I suck at Git so changes will happen soon. Waybagit does properly assign commit dates to match the Wayback Machine snapshot dates. 

# Last Change: 6 August 2021

# Requirements:
# * waybackurls - https://github.com/tomnomnom/waybackurls
# * wgit
# * git

# See also: https://www.youtube.com/watch?v=SYExiynPEKM

# ----------------------------------------------

# Grab all domain URLs
cat domains.txt | waybackurls > wayback.txt

rm cleanwayback.txt

# Only include correct domains in output
while read domain; do
  cat wayback.txt | grep $domain >> cleanwayback.txt
done <domains.txt

# Grab the URLs for all versions of those URLs
cat cleanwayback.txt | waybackurls --get-versions > versions.txt 

# Basic filtering
cat versions.txt | grep -v "http:///$" | grep -v "http://./$" | grep -v "^$" > versions2.txt

# Remove Archive.org from original URL
cat versions2.txt | sed 's/https:\/\/web.archive.org\/web\///' | sed 's/if_//' > cleanversions.txt  

# Extract all dates from all versions
cat cleanversions.txt | awk '{split($0, a, "/"); print a[1] }' | sort -u -r | sed '/^$/d' > dates.txt 

# Create local copy folder and init git repo
mkdir localcopy
git init

while read domain; do
  while read date; do
    cat cleanversions.txt | grep $date | sed "s/^[0-9]*\///g" | grep "$domain" > cleanversions2.txt
    while read fullurl; do
      fullurl_noprotocol=${fullurl:7}
      path_relative=${fullurl_noprotocol#*/}
      filename=`basename ${path_relative%%\?*} >& /dev/null`
      path=`dirname ${path_relative%%\?*} >& /dev/null`
      #echo "Full URL: $fullurl_noprotocol"
      #echo "Filename: $filename"
      #echo "Path: $path"
      #echo "----------"
      mkdir -p localcopy/$domain/$path
      wget "https://web.archive.org/web/${date}if_/${fullurl}" -P localcopy/$domain/$path -q  -N
    done <cleanversions2.txt  
  
    git add --all localcopy/* 
    DATE="${date:0:4}/${date:4:2}/${date:6:2} ${date:8:2}:${date:10:2}:${date:12:2}"
    git commit -m "$domain snapshot from $DATE" --date="$DATE"
  done <dates.txt
done <domains.txt
