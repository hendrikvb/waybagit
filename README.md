# waybagit

Waybagit uses historical Wayback Machine snapshots to build a local Git repository.

Usage:
* Store domain names in domains.txt
* Run script: `# ./waybagit`
* Navigate through Git repository in local folder: `# git log -p`

This script is work in progress. Obviously I suck at Git so changes will happen soon. Waybagit does properly assign commit dates to match the Wayback Machine snapshot dates. 

Last Change: 6 August 2021

# Requirements:
* waybackurls - https://github.com/tomnomnom/waybackurls
* wgit
* git

See also: https://www.youtube.com/watch?v=SYExiynPEKM
